<?php

/**
 * @SWG\Post(path="/calculator",
 *   tags={"CALCULATOR"},
 *   summary="The end point is used to perform the listed operations and it also keep the transaction logs for reporting.",
 *   description="Perform the calculation of listed operands you have to mention ",
 *   operationId="store",
 *   produces={"application/json"},
 *  @SWG\Parameter(
 *     name="operand_x",
 *     in="formData",
 *     description="The first numeric value on which the certain operation is going to be perfomred",
 *     required=true,
 *     type="double"
 *   ),
 *  @SWG\Parameter(
 *     name="operand_y",
 *     in="formData",
 *     description="The second numeric value to perform the required operation. The value will be requied only when operator is add, subtract, divide and multiple",
 *     required=false,
 *     type="integer",
 *   ),
 *  @SWG\Parameter(
 *     name="operator",
 *     in="formData",
 *     description="The ennumiration from the list of followings add, subtract, divide, multiple, sqrt, qubrt, factorial, power",
 *     required=true,
 *     type="string",
 *   ),
 *   @SWG\Response(
 *     response=200,
 *     description="successful operation",
 *     @SWG\Schema(type="string"),
 *     @SWG\Header(
 *       header="X-Rate-Limit",
 *       type="integer",
 *       format="int32",
 *       description="calls per hour allowed by the user"
 *     ),
 *     @SWG\Header(
 *       header="X-Expires-After",
 *       type="string",
 *       format="date-time",
 *       description="date in UTC when toekn expires"
 *     )
 *   ),
 * )
 */
