<?php

/**
 * @SWG\Post(path="/report/history",
 *   tags={"REPORT"},
 *   summary="The end point is used to fetch the report of an interval during which the certain operations are performed.",
 *   description="The reports on the behalf of an interval can be fetched from the system using predefined interval.",
 *   operationId="index",
 *   produces={"application/json"},
 *  @SWG\Parameter(
 *     name="filter",
 *     in="formData",
 *     description="The following filters can be used to fetch the data today, this_week, this_month",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Response(
 *     response=200,
 *     description="successful operation",
 *     @SWG\Schema(type="string"),
 *     @SWG\Header(
 *       header="X-Rate-Limit",
 *       type="integer",
 *       format="int32",
 *       description="calls per hour allowed by the user"
 *     ),
 *     @SWG\Header(
 *       header="X-Expires-After",
 *       type="string",
 *       format="date-time",
 *       description="date in UTC when toekn expires"
 *     )
 *   ),
 * )
 */
