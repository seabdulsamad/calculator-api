<?php

/**
 * @SWG\Swagger(
 *     schemes={"https"},
 *     host="staging.maharati.com/api",
 *     basePath="/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Maharati Application API",
 *         description="An API's detailed list for Maharati awesome application,that will be implemnted in Mobile Applications, Admin Control Panel & Web Application interface.",
 *         @SWG\Contact(
 *             name="Whitehats LLC",
 *             url="https://whitehatsme.com",
 *             email="software@whitehats.ae"
 *         ),
 *         @SWG\License(
 *             name="Apache 2.0",
 *             url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Find about real implementation find below link",
 *         url="http://www.maharati.com"
 *     )
 * )
 */
