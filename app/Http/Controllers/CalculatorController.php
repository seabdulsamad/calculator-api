<?php

namespace App\Http\Controllers;

use App\Helpers\CalcucoHelper;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $data = (new CalcucoHelper())->validate($params)->getResult($params);
        return response()->json($data);
    }
}

