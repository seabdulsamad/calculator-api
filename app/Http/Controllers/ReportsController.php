<?php

namespace App\Http\Controllers;

use App\Helpers\CalcucoHelper;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\History;
use Illuminate\Support\Facades\Validator;

class ReportsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $rules = [
            'filter'    => 'required|in:today,this_week,this_month'
        ];
        //Apply Validation
        $validator = Validator::make($params, $rules);
        // Abort the validate params on error
        if ($validator->fails()) {
            $errors = $validator->messages()->toArray();
            abort(400,json_encode($errors));
        }
        //Calculate To & From Date
        if($params['filter'] == 'today'){
            $toDate = date('Y-m-d H:i:s',strtotime('today'));
            $fromDate = $toDate;
        }else if($params['filter'] == 'this_week'){
            $toDate = date('Y-m-d H:i:s',strtotime('first day of this week'));
            $fromDate = date('Y-m-d H:i:s',strtotime('last day of this week'));
        }else if($params['filter'] == 'this_month'){
            $toDate = date('Y-m-d H:i:s',strtotime('first day of this month'));
            $fromDate = date('Y-m-d H:i:s',strtotime('last day of this month'));
        }
        $data = (new History())->getHistory($toDate,$fromDate);
        return response()->json($data);
    }
}

