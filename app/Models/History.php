<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = "History";
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'operand_x',
        'operand_y',
        'operator',
        'result'
    ];

    /**
     * @param $fromDate
     * @param $toDate
     * @return mixed
     */
    public function getHistory($fromDate, $toDate){
        return $this->whereBetween('created_at', [$fromDate, $toDate])->get();
    }
}
