<?php

namespace App\Helpers;

class JsonHelper {

    /**
     * @param string $jsonString
     * @return bool
     */
    public static function isJson(string $jsonString) : bool
    {
        json_decode($jsonString);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}