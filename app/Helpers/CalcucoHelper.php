<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Validator;
use App\Models\History;

/**
 * @property string cacheKey
 * @property int cacheTtl
 */
class CalcucoHelper
{
    /**
     * @param $params
     * @return float|int
     */
    public function getResult($params)
    {
        $result = 0;
        switch($params['operator']){
            case 'add':
                $result = $params['operand_x'] + $params['operand_y'];
                break;
            case 'subtract':
                $result = $params['operand_x'] - $params['operand_y'];
                break;
            case 'multiple':
                $result = $params['operand_x'] * $params['operand_y'];
                break;
            case 'divide':
                $result = $params['operand_x'] / $params['operand_y'];
                break;
            case 'sqrt':
                $result = sqrt($params['operand_x']);
                break;
            case 'qubrt':
                $result = pow($params['operand_x'],1/3);
                break;
            case 'power':
                $result = pow($params['operand_x'],2);
                break;
            case 'factorial':
                $result = $this->factorial($params['operand_x']);
                break;
        }
        $response = array(
            'operand_x' => $params['operand_x'],
            'operand_y' => $params['operand_y'],
            'operator'  => $params['operator'],
            'result'    => $result
        );
        (new History())->create($response);//Save the History
      return $response;
    }

    /**
     * @param $number
     * @return float|int
     */
    private function factorial($number)
    {
        if($number <= 1){
            return 1;
        }else{
            return $number * $this->factorial($number - 1);
        }
    }
    /**
     * @param $requestParams
     * @return $this
     */
    public function validate($requestParams)
    {
        $rules = [
            'operand_x'      =>  'required',
            'operand_y'      =>  'required_if:operator,==,add,subtract,multiple,divide',
            'operator'       =>  'required|in:add,subtract,multiple,divide,sqrt,qubrt,power,factorial'
        ];

        $validator = Validator::make($requestParams, $rules);
        // Abort the validate pipe on error
        if ($validator->fails()) {
            $errors = $validator->messages()->toArray();
            abort(400,json_encode($errors));
        }

        return $this;
    }
}