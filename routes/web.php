<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['prefix' => 'calculator'], function () use ($router) {
    $router->post('/','CalculatorController@store');
});
$router->group(['prefix' => 'report'], function () use ($router) {
    $router->get('/history','ReportsController@index');
});
