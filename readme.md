# CalcuCo Calculator API

The application provides the REST API's of Calculator with certain set of operations for a well reputed organisation CalcuCo. It provides the list of operations like add, subtract, divide, multiple, factorial, power, square root and cuberoot. The system keeps the logs of each operation performed by the users and you may fetch the reports from and specific period of time.

## Getting Started

The application is developed using the Lumen which is well known PHP framework backed by Laravel. To get started, Clone the project on your machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need to install the followings on your machine in order to setup the development environment on your  machine.

```
PHP >=7.1
```
Install the [composer](https://getcomposer.org/) The dependency manager on your machine to install the project dependencies.
```
PHP composer
```
Install the git on your machine
```
git
```
### Installing

Clone the project using the command below.

```
$ git clone https://bitbucket.org/seabdulsamad/code-challenge.git
```

Install project dependencies by running this command in the project's root directory:

```
$ composer install
```
If you change structure, paths, namespaces, etc., make sure you run the [autoload generator](https://getcomposer.org/doc/03-cli.md#dump-autoload):
```sh
$ composer dump-autoload
```
Run the below command to create the databse architecture.

```
$ php artisan migrate
```

## Built With

* [Lumen](https://lumen.laravel.com/docs/5.6) - The stunningly fast micro-framework by Laravel.
* [Composer](https://getcomposer.org/doc/) - Dependency Management
* MySQL - Database or Storage Container

## Endpoint

The endpoints documentation is also available with swagger. You may check on following URL
http://site-url.com/api/documentation

### CALCULATE

* **URL**
```
http://site-url.com/calculator/
```
* **Method**
```
POST
```
* **Request Filters**
    Below are the supported filters

    | Name | Description |
    | --- | --- |
    | `operand_x` | The numeric operator |
    | `operand_y` | The numeric Operator - will be neglacted once the operator will be factorial, power, sqrt, qubrt |
    | `operator` | Must be from the list - add, subtract, multiple, divide, power, sqrt, qubrt, factorial |

### REPORT

* **URL**
```
http://site-url.com/report/history
```
* **Method**
```
POST
```
* **Request Filters**
    Below are the supported filters

    | Name | Description |
    | --- | --- |
    | `filte` | The string type predefined filters like today, this_week, this_month |

## Authors

* **Abdul Samad** - se.abdulsamad@gmail.com

## License

This project is licensed under the MIT License [Abdul Samad](http://linkedin.com/in/seabdulsamad)